﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Destino
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string pais;

        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }
        private string cordenadax;

        public string Cordenadax
        {
            get { return cordenadax; }
            set { cordenadax = value; }
        }
        private string cordenaday;

        public string Cordenaday
        {
            get { return cordenaday; }
            set { cordenaday = value; }
        }


    }
}
