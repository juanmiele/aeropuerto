﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Pasajero
    {
        private int id_pasajero;

        public int Id_pasajero
        {
            get { return id_pasajero; }
            set { id_pasajero = value; }
        }
        private int pasaporte;

        public int Pasaporte
        {
            get { return pasaporte; }
            set { pasaporte = value; }
        }
        private string nacionalidad;

        public string Nacionalidad
        {
            get { return nacionalidad; }
            set { nacionalidad = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
    }
}
