﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Ticket
    {
        private int id_ticket;

        public int Id_ticket
        {
            get { return id_ticket; }
            set { id_ticket = value; }
        }
        private int id_pasajero;

        public int Id_pasajero
        {
            get { return id_pasajero; }
            set { id_pasajero = value; }
        }
        private int id_vuelo;

        public int Id_vuelo
        {
            get { return id_vuelo; }
            set { id_vuelo = value; }
        }
        private DateTime reserva;

        public DateTime Reserva
        {
            get { return reserva; }
            set { reserva = value; }
        }
    }
}
