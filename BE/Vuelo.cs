﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Vuelo
    {
        private int id_vuelo;

        public int Id_vuelo
        {
            get { return id_vuelo; }
            set { id_vuelo = value; }
        }
        private int origen;

        public int Origen
        {
            get { return origen; }
            set { origen = value; }
        }
        private int destino;

        public int Destino
        {
            get { return destino; }
            set { destino = value; }
        }
        private int escala;

        public int Escala
        {
            get { return escala; }
            set { escala = value; }
        }
        private DateTime salida;

        public DateTime Salida
        {
            get { return salida; }
            set { salida = value; }
        }
        private DateTime llegada;

        public DateTime Llegada
        {
            get { return llegada; }
            set { llegada = value; }
        }
        private int avion;
        public int Avion
        {
            get { return avion; }
            set { avion = value; }
        }
        private bool cancelado;

        public bool  Cancelado
        {
            get { return cancelado; }
            set { cancelado = value; }
        }
        public List<Ticket> tickets = new List<Ticket>();

    }
}
