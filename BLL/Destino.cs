﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Destino
    {
        DAL.MP_Destino mp = new DAL.MP_Destino();

        public void Grabar(BE.Destino destino)
        {
            if (destino.Id == 0)
            {
                mp.Insertar(destino);

            }
            else
            {
                mp.Editar(destino);
            }

        }

        public void Borrar(BE.Destino destino)
        {
            mp.Borrar(destino);
        }

        public List<BE.Destino> Listar()
        {
          return  mp.Listar();

        }



    }
}
