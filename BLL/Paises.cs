﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Paises
    {
        DAL.MP_Paices mp = new DAL.MP_Paices();

        public int BuscarPais(string sigla, List<BE.Paices> paices)
        {
            int index = 0;
            for (int m = 0; m < paices.Count; m++)
            {
                if (sigla == paices[m].Abc)
                {
                    index = m;
                }
            }
            return index;
        }

        public List<BE.Paices> Listar()
        {
          return  mp.Listar();

        }
        



    }
}
