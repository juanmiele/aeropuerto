﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Pasajero
    {
        DAL.MP_Pasajero mp = new DAL.MP_Pasajero();

        public void Grabar(BE.Pasajero pasajero)
        {
            if (pasajero.Id_pasajero == 0)
            {
                mp.Insertar(pasajero);

            }
            else
            {
                mp.Editar(pasajero);
            }

        }

        public void Borrar(BE.Pasajero pasajero)
        {
            mp.Borrar(pasajero);
        }

        public List<BE.Pasajero> Listar()
        {
          return  mp.Listar();

        }



    }
}
