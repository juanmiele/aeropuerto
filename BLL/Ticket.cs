﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Ticket
    {
        DAL.MP_Ticket mp = new DAL.MP_Ticket();

        public void Grabar(BE.Ticket ticket)
        {
            if (ticket.Id_ticket == 0)
            {
                mp.Insertar(ticket);

            }
            else
            {
                mp.Editar(ticket);
            }

        }

        public void Borrar(BE.Ticket ticket)
        {
            mp.Borrar(ticket);
        }

        public List<BE.Ticket> Listar()
        {
          return  mp.Listar();

        }



    }
}
