﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Vuelo
    {
        DAL.MP_Vuelo mp = new DAL.MP_Vuelo();

        public void Grabar(BE.Vuelo Vuelo)
        {
            if (Vuelo.Id_vuelo == 0)
            {
                mp.Insertar(Vuelo);

            }
            else
            {
                mp.Editar(Vuelo);
            }

        }

        public void Borrar(BE.Vuelo Vuelo)
        {
            mp.Borrar(Vuelo);
        }

        public List<BE.Vuelo> Listar()
        {
          return  mp.Listar();

        }



    }
}
