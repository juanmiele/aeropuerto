﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
   public  class MP_Destino
    {

        private Acceso acceso = new Acceso();

        public void Insertar( BE.Destino destino)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@ciu", destino.Nombre));
            parameters.Add(acceso.CrearParametro("@pai", destino.Pais));
            parameters.Add(acceso.CrearParametro("@cx", destino.Cordenadax));
            parameters.Add(acceso.CrearParametro("@cy", destino.Cordenaday));


            acceso.Escribir("DESTINO_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Destino destino)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@ciu", destino.Nombre));
            parameters.Add(acceso.CrearParametro("@pai", destino.Pais));
            parameters.Add(acceso.CrearParametro("@cx", destino.Cordenadax));
            parameters.Add(acceso.CrearParametro("@cy", destino.Cordenaday));
            parameters.Add(acceso.CrearParametro("@id", destino.Id));
            acceso.Escribir("DESTINO_MODIFICAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Destino destino)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@id", destino.Id));
            acceso.Escribir("DESTINO_BORRAR", parameters);

            acceso.Cerrar();
        }

        public  List<BE.Destino> Listar()
        {
            List<BE.Destino> lista = new List<BE.Destino>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("listarDestino");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Destino p = new BE.Destino();
                p.Id = int.Parse(registro[0].ToString());
                p.Nombre = registro[1].ToString();
                p.Pais = registro[2].ToString();
                p.Cordenadax = registro[3].ToString();
                p.Cordenaday = registro[4].ToString();
                lista.Add(p);
            }
            return lista;
        }
    }
}
