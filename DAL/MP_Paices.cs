﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
   public  class MP_Paices
    {

        private Acceso acceso = new Acceso();



        public  List<BE.Paices> Listar()
        {
            List<BE.Paices> lista = new List<BE.Paices>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("listarPaises");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Paices p = new BE.Paices();
                p.Abc = registro[0].ToString();
                p.Pais = registro[1].ToString();
                lista.Add(p);
            }
            return lista;
        }
    }
}
