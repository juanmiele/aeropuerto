﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
   public  class MP_Pasajero
    {

        private Acceso acceso = new Acceso();

        public void Insertar( BE.Pasajero pasajero )
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@ape", pasajero.Apellido));
            parameters.Add(acceso.CrearParametro("@nom", pasajero.Nombre));
            parameters.Add(acceso.CrearParametro("@pas", pasajero.Pasaporte));
            parameters.Add(acceso.CrearParametro("@nac", pasajero.Nacionalidad));


            acceso.Escribir("PASAJERO_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Pasajero pasajero )
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@id", pasajero.Id_pasajero));
            parameters.Add(acceso.CrearParametro("@ape", pasajero.Apellido));
            parameters.Add(acceso.CrearParametro("@nom", pasajero.Nombre));
            parameters.Add(acceso.CrearParametro("@pas", pasajero.Pasaporte));
            parameters.Add(acceso.CrearParametro("@nac", pasajero.Nacionalidad));
            acceso.Escribir("PASAJERO_MODIFICAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Pasajero pasajero)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@id", pasajero.Id_pasajero));
            acceso.Escribir("PASAJERO_BORRAR", parameters);

            acceso.Cerrar();
        }

        public  List<BE.Pasajero> Listar()
        {
            List<BE.Pasajero> lista = new List<BE.Pasajero>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("listarPasajeros");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Pasajero p = new BE.Pasajero();
                p.Id_pasajero = int.Parse(registro[0].ToString());
                p.Nombre = registro[1].ToString();
                p.Apellido = registro[2].ToString();
                p.Pasaporte = int.Parse(registro[3].ToString());
                p.Nacionalidad= registro[4].ToString();

                lista.Add(p);
            }
            return lista;
        }
    }
}
