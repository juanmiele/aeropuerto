﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
   public  class MP_Ticket
    {

        private Acceso acceso = new Acceso();

        public void Insertar( BE.Ticket ticket)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            //parameters.Add(acceso.CrearParametro("@dni", ticket.Nombre));
            //parameters.Add(acceso.CrearParametro("@nom", ticket.Nombre));
            //parameters.Add(acceso.CrearParametro("@ape", ticket.Nombre));

            acceso.Escribir("ticket_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Ticket ticket)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            //parameters.Add(acceso.CrearParametro("@dni", ticket.Nombre));
            //parameters.Add(acceso.CrearParametro("@ape", ticket.Nombre));
            //parameters.Add(acceso.CrearParametro("@nom", ticket.Nombre));
            //parameters.Add(acceso.CrearParametro("@id", ticket.Id));
            acceso.Escribir("ticket_EDITAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Ticket ticket)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            //parameters.Add(acceso.CrearParametro("@id", ticket.Id));
            acceso.Escribir("ticket_BORRAR", parameters);

            acceso.Cerrar();
        }

        public  List<BE.Ticket> Listar()
        {
            List<BE.Ticket> lista = new List<BE.Ticket>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("ticket_LISTAR");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Ticket p = new BE.Ticket();
                //p.Id = int.Parse(registro[3].ToString());
                //p.Nombre = registro[2].ToString();
                //p.Nombre = registro[1].ToString();
                //p.Id = int.Parse(registro[0].ToString());
                lista.Add(p);
            }
            return lista;
        }
    }
}
