﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
   public  class MP_Vuelo
    {

        private Acceso acceso = new Acceso();

        public void Insertar( BE.Vuelo Vuelo)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();


            parameters.Add(acceso.CrearParametro("@ape", Vuelo.Id_vuelo));

            acceso.Escribir("Vuelo_INSERTAR", parameters);

            acceso.Cerrar();
        }

        public void Editar(BE.Vuelo Vuelo)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();


            parameters.Add(acceso.CrearParametro("@id", Vuelo.Id_vuelo));
            acceso.Escribir("Vuelo_EDITAR", parameters);

            acceso.Cerrar();
        }

        public void Borrar(BE.Vuelo persVueloona)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            //parameters.Add(acceso.CrearParametro("@id", Vuelo.Id_vuelo)));
            acceso.Escribir("Vuelo_BORRAR", parameters);

            acceso.Cerrar();
        }

        public  List<BE.Vuelo> Listar()
        {
            List<BE.Vuelo> lista = new List<BE.Vuelo>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("listarvuelo");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Vuelo p = new BE.Vuelo();
                p.Id_vuelo = int.Parse(registro[0].ToString());
                p.Origen = int.Parse(registro[1].ToString());
                p.Destino = int.Parse(registro[2].ToString());
                p.Escala = int.Parse(registro[3].ToString());
                p.Salida = DateTime.Parse(registro[3].ToString());
                p.Llegada = DateTime.Parse(registro[3].ToString());
                p.Avion = int.Parse(registro[4].ToString());
                p.Cancelado = bool.Parse(registro[5].ToString());

                lista.Add(p);
            }
            return lista;
        }
    }
}
