﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class ABMDestino : Form
    {

        BLL.Destino gestordestino = new BLL.Destino();
        BLL.Paises gestorpais = new BLL.Paises();
        List<BE.Paices> paices = new List<BE.Paices>();

        public ABMDestino()
        {
            InitializeComponent();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void ABMDestino_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            BE.Destino p = new BE.Destino();

            p.Nombre = textBox4.Text;
            int index = listBox1.SelectedIndex;
            if (listBox1.SelectedIndex > -1)
            {
                p.Pais = paices[index].Abc;
            }
            p.Cordenadax = textBox5.Text;
            p.Cordenaday = textBox6.Text;
            gestordestino.Grabar(p);
            Enlazar();
        }
        private void Enlazar()
        {

            paices = gestorpais.Listar();
            foreach (BE.Paices p in paices)
            {
                listBox1.Items.Add(p.Pais);
            }
            dataGridView2.DataSource = null;
            dataGridView2.DataSource = gestordestino.Listar();
        }
        private void Limpiar()
        {
            textBox5.Text = "";
            textBox6.Text = "";
            textBox4.Text = "";
            Enlazar();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox4.Text = dataGridView2.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox5.Text = dataGridView2.Rows[e.RowIndex].Cells[3].Value.ToString();
            textBox6.Text = dataGridView2.Rows[e.RowIndex].Cells[4].Value.ToString();
            listBox1.SetSelected(gestorpais.BuscarPais(dataGridView2.Rows[e.RowIndex].Cells[2].Value.ToString(),paices), true);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            BE.Destino p = new BE.Destino();
            p.Id = int.Parse(textBox1.Text);
            p.Nombre = textBox4.Text;
            int index = listBox1.SelectedIndex;
            if (listBox1.SelectedIndex > -1)
            {
                p.Pais = paices[index].Abc;
            }
            p.Cordenadax = textBox5.Text;
            p.Cordenaday = textBox6.Text;


            gestordestino.Grabar(p);
            Enlazar();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            BE.Destino p = new BE.Destino();

            p.Id = int.Parse(textBox1.Text);

            gestordestino.Borrar(p);
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ubicacionx = textBox5.Text;
            string ubicaciony = textBox6.Text;
            string direccion = "https://www.google.com/maps/@" + ubicacionx + "," + ubicaciony + ",13z/";
            //System.Diagnostics.Process.Start("chrome.exe", direccion);
            System.Diagnostics.Process.Start(direccion, " --new-window");
        }
    }
}
