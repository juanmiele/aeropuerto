﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class ABMPasajero : Form
    {

        BLL.Pasajero gestorPasajeros = new BLL.Pasajero();
        BLL.Paises gestorpaises = new BLL.Paises();
        List<BE.Paices> paices  = new List<BE.Paices>();
        public ABMPasajero()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorPasajeros.Listar();
            paices = gestorpaises.Listar();
            foreach (BE.Paices p in paices)
            {
                listBox1.Items.Add(p.Pais);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            BE.Pasajero p = new BE.Pasajero();
            int index = listBox1.SelectedIndex;
            if (listBox1.SelectedIndex > -1)
            {
                p.Nacionalidad = paices[index].Abc;
            }
            p.Id_pasajero = 0;
            p.Apellido = textBox2.Text;
            p.Nombre = textBox3.Text;
            p.Pasaporte = int.Parse(textBox4.Text);

            gestorPasajeros.Grabar(p);
            Limpiar();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            textBox4.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            listBox1.SetSelected(gestorpaises.BuscarPais(dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString(), paices), true);
        }



        private void button2_Click(object sender, EventArgs e)
        {
            BE.Pasajero p = new BE.Pasajero();
            int index = listBox1.SelectedIndex;
            if (listBox1.SelectedIndex > -1)
            {
                p.Nacionalidad = paices[index].Abc;
            }
            p.Id_pasajero = int.Parse(textBox1.Text);
            p.Apellido = textBox2.Text;
            p.Nombre = textBox3.Text;
            p.Pasaporte = int.Parse(textBox4.Text);

            gestorPasajeros.Grabar(p);
            Limpiar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BE.Pasajero p = new BE.Pasajero();

            p.Id_pasajero= int.Parse(textBox1.Text);

            gestorPasajeros.Borrar(p);
            Enlazar();
        }
        private void Limpiar()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            Enlazar();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
