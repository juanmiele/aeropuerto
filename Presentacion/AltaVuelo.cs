﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class AltaVuelo : Form
    {
        BLL.Destino gestordestino = new BLL.Destino();
        BLL.Vuelo gestorvuelo = new BLL.Vuelo();
        List<BE.Destino> destinos = new List<BE.Destino>();
        public AltaVuelo()
        {
            InitializeComponent();
        }

        private void AltaVuelo_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void Enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorvuelo.Listar();
            destinos = gestordestino.Listar();
            foreach (BE.Destino p in destinos)
            {
                listBox1.Items.Add(p.Nombre);
                listBox2.Items.Add(p.Nombre);
                listBox3.Items.Add(p.Nombre);
            }
            listBox4.Items.Add("Boeing 777");
        }
    }
}
