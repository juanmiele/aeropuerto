﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void aBMPasajerosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void aBMDestinoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ABMDestino mostrarabmdestino = new ABMDestino();
            mostrarabmdestino.Show();
        }

        private void altaVuelosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AltaVuelo mostraraltavuelo = new AltaVuelo();
            mostraraltavuelo.Show();
        }

        private void aBMPasajerosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ABMPasajero mostrarabmpasajeros = new ABMPasajero();
            mostrarabmpasajeros.Show();
        }
    }
}
