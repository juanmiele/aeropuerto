USE [Aeropuerto]
GO

/****** Object:  StoredProcedure [dbo].[listarDestino]    Script Date: 17/11/2020 23:45:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create PROC [dbo].[listarDestino]
as 
begin

select * from Aeropuerto.dbo.Destino


END
GO

USE [Aeropuerto]
GO

/****** Object:  StoredProcedure [dbo].[listarPaises]    Script Date: 17/11/2020 23:45:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[listarPaises]
as 
begin

select * from [Aeropuerto].[dbo].[paises]


END
GO

USE [Aeropuerto]
GO

/****** Object:  StoredProcedure [dbo].[listarPasajeros]    Script Date: 17/11/2020 23:45:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[listarPasajeros]
as 
begin

select * from Aeropuerto.dbo.Pasajero


END
GO
USE [Aeropuerto]
GO

/****** Object:  StoredProcedure [dbo].[PASAJERO_BORRAR]    Script Date: 17/11/2020 23:45:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[PASAJERO_BORRAR]
@id int
as
begin

delete from Pasajero 
where
Id_pasajero = @id


END
GO
USE [Aeropuerto]
GO

/****** Object:  StoredProcedure [dbo].[PASAJERO_INSERTAR]    Script Date: 17/11/2020 23:45:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[PASAJERO_INSERTAR]
           @ape char(10),
           @nom char(10),
		   @pas int,
		   @nac char(3)

as
BEGIN 
declare @Id int


set @Id  = isnull( (SELECT MAX(Id_pasajero) from Pasajero) ,0) +1

insert into Pasajero values (@id,@ape,@nom,@pas,@nac)


END
GO
USE [Aeropuerto]
GO

/****** Object:  StoredProcedure [dbo].[PASAJERO_MODIFICAR]    Script Date: 17/11/2020 23:45:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[PASAJERO_MODIFICAR]
			@id int,
           @ape char(10),
           @nom char(10),
		   @pas int,
		   @nac char(10)
as
BEGIN 



update Aeropuerto.dbo.Pasajero set 
Apellido=@ape,
Nombre = @nom,
Pasaporte = @pas,
Nacionalidad = @nac


where
Id_pasajero = @id


END
GO





