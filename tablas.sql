USE [Aeropuerto]
GO

/****** Object:  Table [dbo].[Avion]    Script Date: 17/11/2020 23:43:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Avion](
	[Id_Avion] [int] NOT NULL,
	[Tipodeavion] [nchar](10) NULL,
	[Id_detalleavion] [int] NULL,
	[Capacidad] [nchar](10) NULL,
	[habilitado] [bit] NULL,
 CONSTRAINT [PK_Avion] PRIMARY KEY CLUSTERED 
(
	[Id_Avion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


USE [Aeropuerto]
GO

/****** Object:  Table [dbo].[Destino]    Script Date: 17/11/2020 23:44:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Destino](
	[Id_destino] [int] NOT NULL,
	[Ciudad] [nchar](20) NULL,
	[Pais] [nchar](10) NULL,
	[CordenadaX] [nchar](10) NULL,
	[CordenadaY] [nchar](10) NULL,
 CONSTRAINT [PK_Destino] PRIMARY KEY CLUSTERED 
(
	[Id_destino] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [Aeropuerto]
GO

/****** Object:  Table [dbo].[paises]    Script Date: 17/11/2020 23:44:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[paises](
	[Abreviatura] [nvarchar](255) NULL,
	[pais] [nvarchar](255) NULL
) ON [PRIMARY]
GO

USE [Aeropuerto]
GO

/****** Object:  Table [dbo].[Pasajero]    Script Date: 17/11/2020 23:44:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Pasajero](
	[ID_Pasajero] [int] NOT NULL,
	[Nombre] [nchar](10) NULL,
	[Apellido] [nchar](10) NULL,
	[Pasaporte] [int] NULL,
	[Nacionalidad] [nchar](3) NULL,
 CONSTRAINT [PK_Pasajero] PRIMARY KEY CLUSTERED 
(
	[ID_Pasajero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [Aeropuerto]
GO

/****** Object:  Table [dbo].[Ticket]    Script Date: 17/11/2020 23:44:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Ticket](
	[Id_Ticket] [int] NULL,
	[Id_Pasajero] [int] NULL,
	[Id_Vuelo] [int] NULL,
	[fechadelareserva] [datetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_Pasajero] FOREIGN KEY([Id_Pasajero])
REFERENCES [dbo].[Pasajero] ([ID_Pasajero])
GO

ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_Pasajero]
GO

ALTER TABLE [dbo].[Ticket]  WITH CHECK ADD  CONSTRAINT [FK_Ticket_Vuelo] FOREIGN KEY([Id_Vuelo])
REFERENCES [dbo].[Vuelo] ([Id_vuelo])
GO

ALTER TABLE [dbo].[Ticket] CHECK CONSTRAINT [FK_Ticket_Vuelo]
GO

USE [Aeropuerto]
GO

/****** Object:  Table [dbo].[Vuelo]    Script Date: 17/11/2020 23:44:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Vuelo](
	[Id_vuelo] [int] NOT NULL,
	[Origen] [int] NULL,
	[Escala] [int] NULL,
	[Destino] [int] NULL,
	[FechaHora_salida] [datetime] NULL,
	[FechaHora_llegada] [datetime] NULL,
	[Avion] [int] NULL,
	[Cancelado] [bit] NULL,
 CONSTRAINT [PK_Vuelo] PRIMARY KEY CLUSTERED 
(
	[Id_vuelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Vuelo]  WITH CHECK ADD  CONSTRAINT [FK_Vuelo_Avion] FOREIGN KEY([Avion])
REFERENCES [dbo].[Avion] ([Id_Avion])
GO

ALTER TABLE [dbo].[Vuelo] CHECK CONSTRAINT [FK_Vuelo_Avion]
GO



